import unittest

import sympy
from sympy import Rational
from sympy import Eq
from sympy.abc import x, y

from substitution import Sub, Sub1


class MyTestCase(unittest.TestCase):
    def test_sub1(self):
        a = Sub1(Rational(1), 4, 3)
        self.assertEqual(a.answer(), 7)

    def test_sub1_2(self):
        q = Rational(1, 4)
        h = Rational(1, 2)
        eight = Rational(8)
        b = Sub1(q, h, eight)
        self.assertEqual(b.element(), Eq(y, q * x + h))
        self.assertEqual(b.answer(), Rational(5, 2))

    def test_sub_sin(self):
        s1 = Sub(sympy.sin(x), sympy.pi / 2)
        self.assertEqual(s1.answer(), 1)

        s2 = Sub(sympy.cos(x), 0)
        self.assertEqual(s2.answer(), 1)
        print(s1, s2)


if __name__ == '__main__':
    unittest.main()

import unittest

from pylatex import Document

import questions


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, False)


class Dummy(questions.Questions):
    def __init__(self, data):
        super().__init__()
        self.data = str(data)

    def element(self):
        return self.data

    def answer(self):
        return self.data

    def answer_latex(self) -> str:
        return self.data

    def question_latex(self) -> str:
        return self.data


if __name__ == '__main__':
    # unittest.main()
    from com.kotori316.questionmaker.MakePDF import make
    from questions.quadratic import *

    q = [Quadratic4() for _ in range(40)]
    qs = [[]]
    for a in q:
        qs[0].append(a)
        qs[0].append(Dummy(
            a.b * a.c - a.a.p
        ))
    make(Document("Test_quadratic5",
                  font_size="LARGE",
                  document_options=["fleqn", "twocolumn"],
                  geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"]),
         "Test_quadratic5", qs)

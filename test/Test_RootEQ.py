import unittest

from sympy import sqrt

from root2 import RootEQ1


class MyTestCase(unittest.TestCase):
    def test_7322(self):
        a, b, n, m = 7, 3, 2, 2
        req = RootEQ1(a, b, n, m)
        self.assertEqual(req.answer(), sqrt(21) * 2 + 2)
        ques = req.question_latex()
        self.assertTrue("sqrt{%s}" % a in ques)
        self.assertTrue("sqrt{%s}" % (b * n ** 2) in ques)
        self.assertTrue("sqrt{%s}" % (a * m ** 2) in ques)
        self.assertTrue("sqrt{%s}" % b in ques)

    def test_(self):
        self.assertTrue(True)
        pass


if __name__ == '__main__':
    unittest.main()

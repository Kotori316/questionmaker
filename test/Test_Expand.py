import unittest

from sympy import Rational
from sympy.abc import x

from expand import Expand2


class MyTestCase(unittest.TestCase):
    def test_one(self):
        one = Rational(1)
        self.assertEqual(one * (x + one) + one * (x + one), 2 * x + 2)

    def test_something(self):
        one = Rational(1)
        a = Expand2(one, one, one, one)
        self.assertEqual(a.answer(), 2 * x + 2)
        b = Expand2(4, 2, 3, 2)
        self.assertEqual(b.answer(), 7 * x + 14)

    def test_minus(self):
        c = Expand2(1, -1, -1, 2)
        self.assertEqual(c.answer(), -3)


if __name__ == '__main__':
    unittest.main()

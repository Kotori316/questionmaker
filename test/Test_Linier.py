import unittest

from pylatex import Document
from sympy import Rational

import questions
from linear import LinearFunction1
from questions import Helper


class Dummy(questions.Questions):
    def __init__(self, data):
        super().__init__()
        self.data = str(data)

    def element(self):
        return self.data

    def answer(self):
        return self.data

    def answer_latex(self) -> str:
        return self.data

    def question_latex(self) -> str:
        return self.data


def make():
    from com.kotori316.questionmaker.MakePDF import make

    # q = []
    # for _ in range(15):
    #     a = Polynomial2()
    #     b = Polynomial2()
    #     while a.a == b.a:
    #         a = Polynomial2()
    #         b = Polynomial2()
    #     q.append(Polynomial3(a.eq(), b.eq()))
    #
    # for _ in range(15):
    #     a = Quadratic1()
    #     q.append(Polynomial3(x ** 2, -a.element() + x ** 2))
    # qs = [[]]
    # for a in q:
    #     qs[0].append(a)
    qs = [[LinearFunction1(Helper.rand_int(), Helper.rand_int(), Helper.rand_int()) for _ in range(10)] for _ in
          range(3)]

    make(Document("Test_Linier",
                  font_size="LARGE",
                  document_options=["fleqn", "twocolumn"],
                  documentclass="ujarticle",
                  geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"]),
         "Test_Linier", qs, jp=True)


class MyTestCase(unittest.TestCase):
    def test_1(self):
        a = LinearFunction1(2, 1, 3)
        self.assertEqual(a.answer(), 1)

    def test_2(self):
        a = LinearFunction1(Rational(2, 5), 5, 4)
        self.assertEqual(a.answer(), 2)


if __name__ == '__main__':
    make()
    unittest.main()

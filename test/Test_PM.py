import unittest

from calculation import PMCalc


class MyTestCase(unittest.TestCase):
    def test_something(self):
        from sympy import Rational
        p1 = PMCalc(Rational(-1, 4), Rational(-4, 7), Rational(3, 2), False)
        self.assertEqual(p1.answer(), Rational(-51, 28))
        p2 = PMCalc(Rational(-1, 4), Rational(-4, 7), Rational(3, 2), True)
        self.assertEqual(p2.answer(), Rational(51, 28))

        p3 = PMCalc(Rational(-4), Rational(2), Rational(3), True)
        self.assertEqual(p3.answer(), Rational(-3))
        p4 = PMCalc(Rational(-4), Rational(2), Rational(3), False)
        self.assertEqual(p4.answer(), Rational(3))

        p5 = PMCalc(Rational(1), Rational(2), Rational(3), True)
        p6 = PMCalc(Rational(1), Rational(2), Rational(3), False)
        self.assertEqual(p5.answer(), Rational(2))
        self.assertEqual(p6.answer(), Rational(0))

    def test_allfrac(self):
        for _ in range(20):
            s = PMCalc.make()
            self.assertTrue(s.a.is_Rational)
            self.assertTrue(s.b.is_Rational)
            self.assertTrue(s.c.is_Rational)


if __name__ == '__main__':
    unittest.main()

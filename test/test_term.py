from unittest import TestCase

from sympy import Number

from calc2 import Term

LEFT_BRACKET = r"\left("
RIGHT_BRACKET = r"\right)"


class TestTerm(TestCase):
    def test_number(self):
        t1 = Term(Number(6))
        self.assertEqual("6", t1.latex(True))
        self.assertEqual("+6", t1.latex())
        t2 = Term(Number(-6))
        self.assertEqual("-6", t2.latex(True))
        t3 = Term(Number(6), False)
        self.assertEqual("-6", t3.latex(True))
        t4 = Term(Number(6), True, True)
        self.assertEqual(LEFT_BRACKET + "+6" + RIGHT_BRACKET, t4.latex(True))
        t5 = Term(Number(-6), False, use_bracket=True)
        self.assertEqual(f"-{LEFT_BRACKET}-6{RIGHT_BRACKET}", t5.latex())

    def test_expr(self):
        from sympy.abc import x
        t1 = Term(x ** 2)
        t2 = Term(x ** 2 - 4 * x, False)
        self.assertEqual(x ** 2, t1.value())
        self.assertEqual(-x ** 2 + 4 * x, t2.value())
        t3 = Term(x ** 2 + 3 * x, True, True)
        self.assertEqual(f"{LEFT_BRACKET}x^{'{2}'} + 3 x{RIGHT_BRACKET}", t3.latex(True))

import unittest

from sympy import Number

from calc2 import Calc, Term

LEFT_BRACKET = r"\left("
RIGHT_BRACKET = r"\right)"


class MyTestCase(unittest.TestCase):
    def test_term(self):
        t1 = Term(Number(4))
        t2 = Term(Number(-6))
        self.assertEqual(Number(4), t1.value())
        self.assertEqual(Number(-6), t2.value())
        self.assertNotIn("-", t1.latex())
        self.assertNotIn("+", t2.latex())
        self.assertNotIn("+", t1.latex(first=True))

    def test_term2(self):
        t3 = Term(Number(4), False)
        t4 = Term(Number(-6), False)
        self.assertEqual(Number(-4), t3.value())
        self.assertEqual(Number(6), t4.value())
        self.assertIn("-", t3.latex())
        self.assertIn("+", t4.latex())
        self.assertEqual("6", t4.latex(True))

    def test_calc2(self):
        from sympy.abc import x, y
        t5 = Term(x)
        t6 = Term(x + y, False, True)
        c = Calc(t5, t6)
        self.assertEqual(-y, c.answer())
        # print(c.question_latex())
        # print(c.answer_latex())

    def test_calc(self):
        t1 = Term(Number(4))
        t2 = Term(Number(-6))
        t3 = Term(Number(4), False)
        t4 = Term(Number(-6), False)

        c1 = Calc(t1, t2, t3, t4)
        self.assertEqual(Number(0), c1.answer())

        t1 = Term(Number(4), use_bracket=True)
        t2 = Term(Number(-6), use_bracket=True)
        t3 = Term(Number(4), False, use_bracket=True)
        t4 = Term(Number(-6), False, use_bracket=True)

        c2 = Calc(t1, t2, t3, t4)
        self.assertEqual(Number(0), c2.answer())

    def test_number(self):
        t1 = Term(Number(6))
        self.assertEqual("6", t1.latex(True))
        self.assertEqual("+6", t1.latex())
        t2 = Term(Number(-6))
        self.assertEqual("-6", t2.latex(True))
        t3 = Term(Number(6), False)
        self.assertEqual("-6", t3.latex(True))
        t4 = Term(Number(6), True, True)
        self.assertEqual(LEFT_BRACKET + "+6" + RIGHT_BRACKET, t4.latex(True))
        t5 = Term(Number(-6), False, use_bracket=True)
        self.assertEqual(f"-{LEFT_BRACKET}-6{RIGHT_BRACKET}", t5.latex())

    def test_expr(self):
        from sympy.abc import x
        t1 = Term(x ** 2)
        t2 = Term(x ** 2 - 4 * x, False)
        self.assertEqual(x ** 2, t1.value())
        self.assertEqual(-x ** 2 + 4 * x, t2.value())
        t3 = Term(x ** 2 + 3 * x, True, True)
        self.assertEqual(f"{LEFT_BRACKET}x^{'{2}'} + 3 x{RIGHT_BRACKET}", t3.latex(True))


if __name__ == '__main__':
    unittest.main()

import unittest

from sympy import Rational

from questions import Helper


class HelperTestCase(unittest.TestCase):

    def test_other(self):
        c = Rational(4)
        d = Rational(-6)
        self.assertEqual("4", Helper.replace_one(c))
        self.assertEqual("-6", Helper.replace_one(d))

    def test_1(self):
        a = Rational(1)
        self.assertEqual(Helper.replace_one(a), "")
        self.assertEqual(Helper.replace_one(a, remove_one=False), "1")
        self.assertEqual(Helper.replace_one(a, signed=True), "+")

    def test_minus1(self):
        b = Rational(-1)
        self.assertEqual(Helper.replace_one(b), "-")
        self.assertEqual(Helper.replace_one(b, remove_one=False), "-1")
        self.assertEqual(Helper.replace_one(b, signed=True), "-")


if __name__ == '__main__':
    unittest.main()

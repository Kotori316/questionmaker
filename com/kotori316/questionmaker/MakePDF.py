import os
from typing import List

from pylatex import Alignat, Document, NoEscape, Section

from questions import Questions


def make(doc: Document, name: str, list_list_question: List[List[Questions]], line: str = "\\clearpage", jp=False):
    doc.append(NoEscape(r"\allowdisplaybreaks[1]"))

    for c1 in range(len(list_list_question)):
        q_list: List[Questions] = list_list_question[c1]
        with doc.create(Section(NoEscape(fr"Question {c1 + 1}"), numbering=False)):
            with doc.create(Alignat(aligns=1, numbering=False)) as align:
                for i1 in range(len(q_list)):
                    f: Questions = q_list[i1]
                    q: str = f.question_latex()
                    if i1 == len(q_list) - 1:
                        align.append(NoEscape(fr"({i1 + 1}) & \ {q}"))
                    else:
                        align.append(NoEscape(fr"({i1 + 1}) & \ {q} \\ \\"))
            doc.append(NoEscape(line))

    for c2 in range(len(list_list_question)):
        q_list: List[Questions] = list_list_question[c2]
        with doc.create(Section(f"Answer {c2 + 1}", numbering=False)):
            with doc.create(Alignat(aligns=1, numbering=False)) as align:
                for i2 in range(len(q_list)):
                    f: Questions = q_list[i2]
                    a: str = f.answer_latex()
                    if i2 == len(q_list) - 1:
                        align.append(NoEscape(fr"({i2 + 1}) & \ {a}"))
                    else:
                        align.append(NoEscape(fr"({i2 + 1}) & \ {a} \\ \\"))
            doc.append(NoEscape(line))

    if jp:
        doc.generate_pdf(name, clean_tex=True, compiler="latexmk", compiler_args=[])
        os.remove(name + ".dvi")
        os.remove(name + ".synctex.gz")
    else:
        doc.generate_pdf(name, clean_tex=True)

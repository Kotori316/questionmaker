from pylatex import Document

from com.kotori316.questionmaker import MakePDF
from root2 import RootEQ1


def pdf(name: str, paper: int, count: int):
    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    elements = [[RootEQ1.make() for _ in range(count)] for _ in range(paper)]
    MakePDF.make(doc, name, elements)


if __name__ == '__main__':
    for t in range(20):
        pdf(f"Root5 No.{t + 1}", 1, 5)

from pylatex import Document

from com.kotori316.questionmaker import MakePDF
from func import InverseProportion, Proportion, QuadraticProportion
from questions import Helper


def pdf(name: str, paper: int):
    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   documentclass="ujarticle",
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    elements = [qs() for _ in range(paper)]
    MakePDF.make(doc, name, elements, jp=True)


def qs():
    q = [
        Proportion(Helper.rand_int(), Helper.rand_int(), Helper.rand_int()),
        Proportion(Helper.rand_int(), Helper.rand_frac(), Helper.rand_int()),
        Proportion(Helper.rand_frac(), Helper.rand_int(), Helper.rand_int()),
        Proportion(Helper.rand_int(), Helper.rand_int(), Helper.rand_frac()),
        InverseProportion.make(),
        InverseProportion.make(),
        InverseProportion(Helper.rand_int(), Helper.rand_frac(), Helper.rand_int()),
        InverseProportion(Helper.rand_frac(), Helper.rand_int(), Helper.rand_int()),
        QuadraticProportion.make(),
        QuadraticProportion.make(),

    ]
    return q


if __name__ == '__main__':
    for t in range(5):
        pdf(f"Proportions No.{t + 1}", 6)

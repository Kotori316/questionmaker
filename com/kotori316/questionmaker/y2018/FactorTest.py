import re

from pylatex import Alignat, Document, NoEscape, Section

from questions import factor


def append(n0: int, appendable: list):
    factor_function = factor.make_factor()
    if n0 == 0:
        return
    if factor_function in appendable:
        append(n0, appendable)
    else:
        appendable.append(factor_function)
        append(n0 - 1, appendable)


def to_latex(func: object) -> str:
    s = str(func)
    s1 = s.replace("**", "^")
    s2 = s1.replace("*", "")
    s3 = re.sub(" ([0-9]+)/([0-9]+)", r" \\frac{\1}{\2}", s2)  # Case of 49 / 6
    s4 = re.sub(" ([0-9]+)([a-zA-Z])/([0-9]+)", r" \\frac{\1}{\3} \2", s3)  # Case of 32x / 5
    s5 = re.sub(" ([a-zA-Z])/([0-9]+)", r" \\frac{1}{\2} \1", s4)  # Case of x / 2
    s6 = s5.replace("(", r" \left( ")  # Don't forget to insert spaces.
    s7 = s6.replace(")", r" \right) ")  # Don't forget to insert spaces.
    return s7


def generate_pdf(filename: str, number_per_paper: int, paper: int = 1, delete_tex: bool = True):
    doc = Document(filename,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"],
                   page_numbers=True,
                   indent=False)
    doc.append(NoEscape(r"\allowdisplaybreaks[1]"))

    factor_elements = []
    for count in range(paper):
        factors = []
        append(number_per_paper, factors)
        factor_elements.append(factors)
        with doc.create(Section(f"Questions {count+1}", label=False, numbering=False)):
            with doc.create(Alignat(aligns=1, numbering=False)) as align:
                for i in range(len(factors)):
                    align.append(NoEscape(fr"({i+1}) & \ {to_latex(factors[i].expand())} \\ \\"))
        doc.append(NoEscape(r"\clearpage"))

    for count in range(paper):
        factors = factor_elements[count]
        with doc.create(Section(f"Answers {count+1}", label=False, numbering=False)):
            with doc.create(Alignat(aligns=1, numbering=False)) as align:
                for i in range(len(factors)):
                    align.append(NoEscape(fr"({i+1}) & \ {to_latex(factors[i].factor())} \\ \\"))
        doc.append(NoEscape(r"\clearpage"))

    doc.generate_pdf(filename, clean_tex=delete_tex)


if __name__ == '__main__':
    n = 20
    p = 5
    for x in range(3):
        generate_pdf(f"因数分解{n}問_{p}セット_No.{x+1}", number_per_paper=n, paper=p, delete_tex=False)
    for x in range(10):
        generate_pdf(f"因数分解{n}問_No.{x+1}", number_per_paper=n)

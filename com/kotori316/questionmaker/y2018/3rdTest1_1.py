from typing import List

from pylatex import Document

import questions
from kotori316.questionmaker import MakePDF


def make(name: str, c: int):
    q_list_list = []
    for _ in range(c):
        q_list: List[questions.Questions] = \
            [questions.IntCalc(), questions.FracCalc(), questions.FracCalc(), questions.Cancel(),
             questions.RootCalc(), questions.RootCalc(), ]

        e1126 = [
            questions.Expand(questions.factor.Factor1()),
            questions.Expand(questions.factor.Factor1()),
            questions.Expand(questions.factor.Factor2()),
            questions.Expand(questions.factor.Factor6()),
            questions.Expand1(),
            questions.Expand1(),
        ]

        f126 = [
            questions.factor.Factor1(), questions.factor.Factor1(),
            questions.factor.Factor1(), questions.factor.Factor1(),
            questions.factor.Factor2(), questions.factor.Factor2(),
            questions.factor.Factor6(), questions.factor.Factor6(),
        ]
        import random
        random.shuffle(e1126)
        random.shuffle(f126)

        q_list.extend(e1126)
        q_list.extend(f126)
        q_list_list.append(q_list)

    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


if __name__ == '__main__':
    for x in range(5):
        make(f"因数分解 No.{x+1}", 6)

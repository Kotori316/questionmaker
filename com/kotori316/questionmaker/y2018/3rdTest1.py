import random
from typing import List

from pylatex import Document

import questions
from questions import calculation, cancel, expand, factor
from com.kotori316.questionmaker import MakePDF


def make(name: str, n: int):
    q_list_list = []
    for _ in range(n):
        q_list: List[questions.Questions] = \
            [calculation.IntCalc(), calculation.FracCalc(), cancel.Cancel(),
             expand.Expand1(), calculation.RootCalc()]

        for i1 in range(2):
            q_list.append(expand.Expand(factor.make_factor()))

        q_list.append(expand.Expand(random.choice((
            factor.Factor3, factor.Factor4,
            factor.Factor5, factor.Factor6
        ))()))

        for i2 in range(3):
            q_list.append(factor.Factor1())

        fs = (factor.Factor2, factor.Factor3, factor.Factor4, factor.Factor5,
              factor.Factor6)
        for i3 in range(3):
            q_list.append(random.choice(fs)())
        q_list_list.append(q_list)

    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


if __name__ == '__main__':
    for x in range(5):
        make(f"小テスト 5セット No.{x + 1}", 5)

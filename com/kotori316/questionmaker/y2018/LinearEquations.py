from pylatex import Document

from kotori316.questionmaker import MakePDF
from linear import Linear2


def make(name: str):
    q_list_list = []
    for x in range(5):
        q_list_list.append(
            [Linear2() for _ in range(15)]
        )
    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


if __name__ == '__main__':
    make("o")

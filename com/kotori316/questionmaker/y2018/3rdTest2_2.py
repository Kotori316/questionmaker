from pylatex import Document
from sympy.abc import x

import questions
from kotori316.questionmaker import MakePDF


def make(name, count):
    q_list_list = []
    for _ in range(count):
        q_list = []
        for _ in range(10):
            a = questions.linear.Polynomial2()
            b = questions.linear.Polynomial2()
            while a.a == b.a:
                a = questions.linear.Polynomial2()
                b = questions.linear.Polynomial2()
            q_list.append(questions.linear.Polynomial3(a.eq(), b.eq()))

        for _ in range(5):
            a = questions.quadratic.Quadratic1()
            q_list.append(questions.Equation(x ** 2, -a.element() + x ** 2))

        for _ in range(5):
            a = questions.quadratic.Quadratic4()
            q_list.append(questions.Equation(x ** 2, (-a.element() / a.c) + x ** 2))
        q_list_list.append(q_list)
        doc = Document(name,
                       font_size="LARGE",
                       document_options=["fleqn", "twocolumn"],
                       geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

        MakePDF.make(doc, name, q_list_list)


if __name__ == '__main__':
    for n in range(5):
        make(f"交点 No.{n+1}", 6)

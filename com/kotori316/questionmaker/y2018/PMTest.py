from pylatex import Document

from calculation import IntCalc, PMCalc
from com.kotori316.questionmaker import MakePDF
from expand import Expand2
from func import InverseProportion, Proportion, QuadraticProportion
from linear import Polynomial4, Polynomial5
from quadratic import Quadratic2
from questions import Helper
from cancel import Cancel
from root import Root2
from root2 import RootEQ1
from substitution import Sub1, Sub2


def pdf(name: str, paper: int):
    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   documentclass="ujarticle",
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    elements = [qpaper() for _ in range(paper)]
    MakePDF.make(doc, name, elements, jp=True)


def qpaper():
    q = [
        IntCalc(),
        IntCalc(),
        PMCalc.make(),
        Root2(),
        RootEQ1.make(),
        Sub1(Helper.rand_int(), Helper.rand_int(), Helper.rand_int()),
        Sub1(Helper.rand_int(), Helper.rand_frac(), Helper.rand_frac()),
        Sub2(Helper.rand_frac(), Helper.rand_int()),
        Cancel(),
        Cancel(),
        Proportion(Helper.rand_int(), Helper.rand_int(), Helper.rand_int()),
        InverseProportion(Helper.rand_int(), Helper.rand_int(), Helper.rand_int()),
        QuadraticProportion.make(),
        Polynomial4(Helper.rand_frac(), Helper.rand_int(), Helper.rand_int()),
        Polynomial5(Helper.rand_int(), Helper.rand_frac(), Helper.rand_int()),
        Expand2(Helper.rand_int(), Helper.rand_int(), Helper.rand_int(), Helper.rand_int()),
        Expand2(Helper.rand_int(), Helper.rand_frac(), Helper.rand_frac(), Helper.rand_int()),
        Quadratic2(),
    ]
    return q


if __name__ == '__main__':
    for t in range(5):
        pdf(f"PM No.{t + 1}", 1)

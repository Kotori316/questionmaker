# Copyright (c) 2018 Kotori316
#
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

import random
from typing import List

from pylatex import *
from sympy import *
from sympy.abc import x

ALL_ELEMENT: int = 75

NUMBERS0 = list(range(5))
NUMBERS0.extend((1, 1, 1, 1, 1, 1,))
NUMBERS1: List[int] = [i1 for i1 in range(-9, 9 + 1) if i1 != 0]
NUMBERS2: List[int] = [i2 for i2 in range(-20, 20 + 1) if i2 != 0]


def to_latex(func: object) -> str:
    s = str(func)
    s1 = s.replace("**", "^")
    s2 = s1.replace("*", "")
    return s2


def get_rand_element(l):
    return l[random.randint(0, len(l) - 1)]


def make_number_list(count: int, appendable: list, c0, c1: list, c2: list):
    if count == 0:
        return appendable
    r = get_rand_element(c0)
    a, c = 1, 1
    if r == 1:
        b, d = get_rand_element(c1), get_rand_element(c2)
    elif r == 0:
        r = 1
        a = get_rand_element(range(2, 5))
        b = get_rand_element(c1)
        c, d = a, b
    else:
        b, d = get_rand_element(c1), get_rand_element(c2)

    b, d = min(b, d), max(b, d)
    t = (r, a, b, c, d)
    if t in appendable:
        return make_number_list(count, appendable, c0, c1, c2)
    else:
        appendable.append(t)
        return make_number_list(count - 1, appendable, c0, c1, c2)


def make_pdf(name):
    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"],
                   page_numbers=True,
                   indent=False)
    doc.append(NoEscape(r"\allowdisplaybreaks[1]"))

    factor_elements: List = [r * (a * x + b) * (c * x + d) for (r, a, b, c, d) in
                             make_number_list(ALL_ELEMENT, [], NUMBERS0, NUMBERS1, NUMBERS2)]

    with doc.create(Section("Question", label=False)):
        with doc.create(Alignat(aligns=1, numbering=False)) as align:
            for i in range(len(factor_elements)):
                align.append(NoEscape(fr"({i+1}) & \ {to_latex(expand(factor_elements[i]))} \\ \\"))

    doc.append(NoEscape(r"\clearpage"))

    with doc.create(Section("Answers", label=False)):
        with doc.create(Alignat(aligns=1, numbering=False)) as align:
            for i in range(len(factor_elements)):
                align.append(NoEscape(fr"({i+1}) & \ {to_latex(factor(factor_elements[i]))} \\ \\"))

    doc.generate_pdf(name, clean_tex=False)


if __name__ == '__main__':
    make_pdf("no1")
    make_pdf("no2")

import random

from pylatex import Alignat, Document, NoEscape, Section

ALL_ELEMENT = 25


def sign(n: int):
    return "+" if n >= 0 else "-"


def make_number_list(n: int, l: list, c1: list, c2: list):
    if n == 0:
        return l
    t, s = c1[random.randint(0, len(c1) - 1)], c2[random.randint(0, len(c2) - 1)]
    t, s = max(t, s), min(t, s)
    if (t, s) in l:
        return make_number_list(n, l, c1, c2)
    else:
        l.append((t, s))
        return make_number_list(n - 1, l, c1, c2)


doc = Document("Doc", font_size="LARGE", document_options="fleqn",
               geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

numbers1 = [x for x in range(-9, 9 + 1) if x != 0]
numbers2 = [x for x in range(-20, 20 + 1) if x != 0]
numbers = make_number_list(ALL_ELEMENT, [], numbers1, numbers2)

with doc.create(Section("Question")):
    with doc.create(Alignat(aligns=1, numbering=False)) as align:
        for x in range(len(numbers)):
            a, b = numbers[x]
            t1 = sign(a + b)
            t2 = sign(a * b)
            if a + b == 1 or a + b == -1:
                align.append(NoEscape(fr"({x + 1}) & \ x^2 {t1} x {t2} {abs(a*b)} \\"))
            elif a + b != 0:
                align.append(NoEscape(fr"({x + 1}) & \ x^2 {t1} {abs(a+b)}x {t2} {abs(a*b)} \\"))
            else:
                align.append(NoEscape(fr"({x + 1}) & \ x^2 {t2} {abs(a * b)} \\"))

doc.append(NoEscape(r"\newpage"))

with doc.create(Section("Answers")):
    with doc.create(Alignat(aligns=1, numbering=False)) as align:
        for x in range(len(numbers)):
            a, b = numbers[x]
            if a == b:
                string = fr"({x+1}) & \ (x {sign(a)} {abs(a)})^2 \\"
            else:
                string = fr"({x+1}) & \ (x {sign(a)} {abs(a)})(x {sign(b)} {abs(b)}) \\"
            align.append(NoEscape(string))

doc.generate_pdf("doc", clean_tex=False)

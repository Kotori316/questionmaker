from pylatex import Document

from com.kotori316.questionmaker import MakePDF
from questions import quadratic


def pdf(name: str, paper: int, count: int):
    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    elements = [[quadratic.make_quadratic() for _ in range(count)] for _ in range(paper)]
    MakePDF.make(doc, name, elements)


if __name__ == '__main__':
    for t in range(5):
        pdf(f"2次方程式 No.{t+1}", 6, 20)

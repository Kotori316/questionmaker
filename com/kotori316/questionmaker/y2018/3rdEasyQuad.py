import random
from typing import List

from pylatex import Document

import questions
from kotori316.questionmaker import MakePDF


def make(name: str, n: int):
    q_list_list = []
    for _ in range(n):
        q_list: List[questions.Questions] = []
        a = set()
        while len(a) < 6:
            a.add(questions.quadratic.Quadratic1())
        b = set()
        while len(b) < 3:
            b.add(questions.quadratic.Quadratic2())
        q_list.extend(a)
        q_list.extend(b)
        q_list.append(questions.quadratic.Quadratic3())
        random.shuffle(q_list)
        q_list_list.append(q_list)

    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


if __name__ == '__main__':
    for x in range(5):
        make(f"2次方程式 簡単 6セット No.{x+1}", 6)

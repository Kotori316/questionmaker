import random
from typing import List

from pylatex import Document

import questions
from com.kotori316.questionmaker import MakePDF


def make(name: str, page: int):
    q_list_list = []
    for _ in range(page):
        # First 5 questions
        q_list: List[questions.Questions] = \
            [questions.IntCalc(), questions.FracCalc(), questions.Cancel(),
             questions.Expand1(), questions.RootCalc()]

        # 10 quadratic questons
        quadratics = [
            questions.quadratic.Quadratic1(),
            questions.quadratic.Quadratic1(),
            questions.quadratic.Quadratic1(),
            questions.quadratic.Quadratic1(),
            questions.quadratic.Quadratic2(),
            questions.quadratic.Quadratic2(),
            questions.quadratic.Quadratic3(),
            questions.quadratic.Quadratic4(),
            questions.quadratic.Quadratic4(),
            questions.quadratic.Quadratic5(),
        ]
        random.shuffle(quadratics)
        q_list.extend(quadratics)
        # total 15
        q_list_list.append(q_list)

    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


if __name__ == '__main__':
    for n in range(5):
        make(f"小テスト 2 No.{n+1}", 6)

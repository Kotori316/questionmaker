from typing import List

from pylatex import Document
from sympy.abc import x

from calc2 import Calc, Term
from kotori316.questionmaker import MakePDF
from questions import Helper


def make(name: str, page: int):
    q_list_list = []
    for _ in range(page):
        # First 5 questions
        q_list: List[Calc] = [create_expr() for _ in range(10)]
        # total 10
        q_list_list.append(q_list)

    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


def rand_bool() -> bool:
    import random as rdm
    return bool(rdm.randint(0, 1))


def create_expr() -> Calc:
    a = [Helper.rand_int() for _ in range(4)]
    b = [Helper.rand_int() for _ in range(4)]
    a_ = [i * x ** c for c, i in enumerate(a)]
    b_ = [i * x ** c for c, i in enumerate(b)]
    x1 = sum(a_)
    x2 = sum(b_)
    import random as rdm
    rdm.shuffle(a_)
    rdm.shuffle(b_)
    c1 = Calc.from_exp_list(a_)
    c2 = Calc.from_exp_list(b_)

    p1 = rand_bool()
    p2 = rand_bool()
    return Calc(Term(x1, p1, True, c1.question_latex()), Term(x2, p2, True, c2.question_latex()))


if __name__ == '__main__':
    for count in range(5):
        make(f"H1Calc1_{count + 1}", 1)

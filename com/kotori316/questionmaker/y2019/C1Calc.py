from typing import List

from pylatex import Document
from sympy import Number

from calc2 import Calc, Term
from kotori316.questionmaker import MakePDF
from questions import Helper


def make(name: str, page: int, pq: int):
    q_list_list = []
    for _ in range(page):
        q_list: List[Calc] = list()
        q_list.extend([create_expr() for _ in range(pq + 1)])
        q_list.extend([create_expr2() for _ in range(pq)])
        q_list.extend([create_expr3() for _ in range(pq)])
        import random as rdm
        rdm.shuffle(q_list)
        q_list_list.append(q_list)

    doc = Document(name,
                   font_size="LARGE",
                   document_options=["fleqn", "twocolumn"],
                   geometry_options=["top=10truemm", "bottom=15truemm", "left=10truemm", "right=10truemm"])

    MakePDF.make(doc, name, q_list_list)


def rand_bool() -> bool:
    import random as rdm
    return bool(rdm.randint(0, 1))


def create_expr() -> Calc:
    a = [Term(Number(Helper.rand_int()), rand_bool()) for _ in range(3)]
    return Calc.from_list(a)


def create_expr2() -> Calc:
    a = [Term(Number(Helper.rand_int()), rand_bool(), True) for _ in range(3)]
    return Calc.from_list(a)


def create_expr3() -> Calc:
    a = [Term(Number(Helper.rand_int()), rand_bool(), rand_bool()) for _ in range(3)]
    return Calc.from_list(a)


if __name__ == '__main__':
    for count in range(5):
        make(f"C1Calc_{count + 1}", 3, 3)

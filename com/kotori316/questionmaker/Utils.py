def to_latex(func: object) -> str:
    s = str(func)
    s1 = s.replace("**", "^")
    s2 = s1.replace("*", "")
    return s2


def latex_print(o: object):
    print(to_latex(o))

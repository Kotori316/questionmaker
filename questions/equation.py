import sympy

from questions import Questions


class Equation(Questions):
    def __init__(self, e1, e2):
        super().__init__()
        self.e1 = e1
        self.e2 = e2

    def element(self):
        return sympy.Eq(self.e1, self.e2)

    def answer(self):
        return sympy.solve(self.element())

    def question_latex(self):
        return f"{sympy.latex(self.e1)} = {sympy.latex(self.e2)}"

    def answer_latex(self):
        a = self.answer()
        return f"x = {', '.join([sympy.latex(t) for t in a])}"

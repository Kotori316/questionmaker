import sympy

from questions.questions import Questions


class RootEQ1(Questions):

    def __init__(self, a: int, b: int, n: int, m: int, l: int):
        super().__init__()
        self.n2 = l
        self.a = a
        self.b = b
        self.n = n
        self.m = m
        self.f = (sympy.sqrt(self.a) + self.n * sympy.sqrt(self.b)) * (
                self.m * sympy.sqrt(self.a) - self.n2 * self.b / sympy.sqrt(self.b))

    def element(self):
        return self.f

    def question_latex(self) -> str:
        left = r"\left("
        right = r"\right)"
        sqrt = r"\sqrt{%s}"
        frac = r"\frac{%s}{%s}" % (self.n2 * self.b, sqrt % self.b)
        return left + (sqrt % self.a) + "+" + (sqrt % (self.b * self.n ** 2)) + right + left + (
                sqrt % (self.a * self.m ** 2)) + "-" + frac + right

    def answer(self):
        return sympy.expand(self.element())

    @staticmethod
    def make():
        import random as rm
        ab = [2, 3, 5, 6, 7, 11]
        a = rm.choice(ab)
        b = rm.choice(ab)
        nm = [2, 3, 4, 5, 6, 7]
        n = rm.choice(nm)
        n2 = rm.choice(nm)
        m = rm.choice(nm)
        return RootEQ1(a, b, n, m, n2)

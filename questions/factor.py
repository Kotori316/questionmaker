import random

import sympy
from sympy.abc import x, y
from sympy.printing import latex

from questions.questions import Questions


def get_rand_element(l):
    return random.choice(l)


class Factor(Questions):
    """
    Abstract class for Factors.
    Element method must be overrided.
    """

    def answer(self):
        return self.factor()

    def question_latex(self):
        return latex(self.expand())

    def element(self):
        raise NotImplementedError("Element method must be implemented.")

    def expand(self):
        return sympy.expand(self.element())

    def factor(self):
        return sympy.factor(self.element())


class Factor1(Factor):
    """
    Case of (x + a)(x + b), including (x + a) ^ 2
    """
    __a1list = [_ for _ in range(-19, 19 + 1) if _ != 0]
    __a2list = [_ for _ in range(-9, 9 + 1) if _ != 0]
    __a1list.extend(__a2list)

    def __init__(self):
        super().__init__()
        a1 = get_rand_element(self.__a1list)
        a2 = self.rand_int()
        self.a1 = min(a1, a2)
        self.a2 = max(a1, a2)

    def element(self):
        return (x + self.a1) * (x + self.a2)

    def __eq__(self, other):
        return type(self) == type(other) and self.a1 == other.a1 and self.a2 == other.a2


class Factor2(Factor):
    """
    Case of a(x + b)(x + c)
    a can be 1.
    """
    __a1list = range(1, 9 + 1)

    def __init__(self):
        super().__init__()
        a1 = get_rand_element(self.__a1list)
        a2 = self.rand_int()
        a3 = self.rand_int()
        self.a1 = a1
        self.a2 = min(a2, a3)
        self.a3 = max(a2, a3)

    def element(self):
        return self.a1 * (x + self.a2) * (x + self.a3)

    def __eq__(self, other):
        return type(self) == type(other) and self.a1 == other.a1 and self.a2 == other.a2 and self.a3 == other.a3


class Factor3(Factor):
    """
    Case of (ax + b)^2
    """

    def __init__(self):
        super().__init__()
        a1 = self.rand_int()
        a2 = self.rand_int()
        while a2 == a1:
            a2 = self.rand_int()
        self.a1 = min(a1, a2)
        self.a2 = max(a1, a2)

    def element(self):
        return (self.a1 * x + self.a2) ** 2

    def __eq__(self, other):
        return type(self) == type(other) and self.a1 == other.a1 and self.a2 == other.a2


class Factor4(Factor):
    """
    Case of (x + y + a)(x + y + b)
    """
    __a1list = [y]
    __a1list.extend([_ for _ in range(-5, 5 + 1) if _ != 0])

    def __init__(self):
        super().__init__()
        a1 = random.choices(self.__a1list, (15, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))[0]
        s1 = random.choice((1, -1))
        a2 = self.rand_int()
        a3 = self.rand_int()
        self.a1 = a1 * s1
        self.a2 = min(a2, a3)
        self.a3 = max(a2, a3)

    def element(self):
        return ((x + self.a1) + self.a2) * ((x + self.a1) + self.a3)

    def expand(self):
        squared = (x + self.a1)
        plus = self.a2 + self.a3
        multipy = self.a2 * self.a3
        sign1 = "+" if plus >= 0 else "-"
        sign2 = "+" if multipy >= 0 else "-"

        if abs(plus) == 1:
            return f"({squared}) ^ 2 {sign1} ({squared}) {sign2} {abs(multipy)}"
        elif plus != 0:
            return f"({squared}) ^ 2 {sign1} {abs(plus)} ({squared}) {sign2} {abs(multipy)}"
        else:
            return f"({squared}) ^ 2 {sign2} {abs(multipy)}"

    def __eq__(self, other):
        return type(self) == type(other) and self.a1 == other.a1 and self.a2 == other.a2 and self.a3 == other.a3


class Factor5(Factor):
    """
    Case of (x + a)(y + b)
    """

    def __init__(self):
        super().__init__()
        self.a1 = self.rand_int()
        self.a2 = self.rand_int()

    def element(self):
        return (x + self.a1) * (y + self.a2)

    def __eq__(self, other):
        return type(self) == type(other) and self.a1 == other.a1 and self.a2 == other.a2


class Factor6(Factor):
    """
    Case of (x + a/b)^2
    """

    def __init__(self):
        super().__init__()
        self.ab = self.rand_frac()

    def element(self):
        return (x + self.ab) ** 2

    def factor(self):
        return self.element()

    def __eq__(self, other):
        return type(self) == type(other) and self.ab == other.ab

    def question_latex(self):
        import re
        a = super().question_latex()
        return re.sub("frac{([0-9]+) (x)}{([0-9])}", "frac{\\1}{\\3} \\2", a)


__FACTORS = (Factor1, Factor2, Factor3, Factor4, Factor5, Factor6)


def make_factor() -> Factor:
    question = random.choices(__FACTORS, (6, 1, 0.5, 0.5, 0.75, 0.75))[0]
    return question()

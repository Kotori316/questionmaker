from typing import Iterable, Optional

from sympy import Add, Expr, latex

from questions import Questions

LEFT_BRACKET = r"\left("
RIGHT_BRACKET = r"\right)"


class Term(object):
    """
    First arg is Expr.
    Second arg determines if plus(+) or minus(-)

    >>> from sympy import Number
    >>> Term(Number(6))
    6
    >>> Term(Number(-6))
    -6
    >>> Term(Number(6), False)
    -6
    >>> Term(Number(6), use_bracket = True)
    (+6)
    >>> Term(Number(-6), False, use_bracket=True)
    -(-6)
    """

    def __init__(self, num: Expr, pm: bool = True, use_bracket: bool = False, q: Optional[str] = None):
        self.q = q
        self.num = num
        self.plus = pm
        self.use_bracket = use_bracket

    def value(self):
        if self.plus:
            return self.num
        else:
            return -self.num

    def latex(self, first=False) -> str:
        def _pm(v, f=first):
            s = latex(v)
            if s.startswith("-") or f or self.num.is_Add:
                return s
            else:
                return "+" + s

        if self.use_bracket:
            if not first:
                pre = "+" if self.plus else "-"
            else:
                pre = "" if self.plus else "-"
            if self.q is not None:
                return pre + LEFT_BRACKET + self.q + RIGHT_BRACKET
            else:
                return pre + LEFT_BRACKET + _pm(latex(self.num), False) + RIGHT_BRACKET
        else:
            if self.q is None:
                return _pm(self.value())
            else:
                return self.q

    def __str__(self):
        if self.use_bracket:
            prefix = "+" if self.plus else "-"
            return f"{prefix}({self.num})"
        else:
            return f"{self.value()}"

    def __repr__(self):
        return self.__str__()


class Calc(Questions):
    """
    t1 + t2 + ... + tn
    """

    def __init__(self, t1: Term, t2: Term, *ts: Term):
        super().__init__()
        self.ts = [t1, t2]
        self.ts.extend(ts)

    def answer(self):
        v = [t.value() for t in self.ts]
        return sum(v)

    def element(self):
        return Add(*map(lambda t: t.value(), self.ts), )

    def question_latex(self):
        s = ""
        for i, v in enumerate(self.ts):
            if not i == 0:
                s += v.latex()
            else:
                s += v.latex(True)
        return s

    @staticmethod
    def from_list(it: Iterable[Term]):
        terms = list(it)
        return Calc(terms[0], terms[1], *[terms[i] for i in range(2, len(terms))])

    @staticmethod
    def from_exp_list(it: Iterable[Expr]):
        return Calc.from_list(map(Term, it))

import random

import sympy
from sympy import latex
from sympy.abc import x, y

from questions import Questions


class Cancel(Questions):
    """
    Case of (a * x ^ b * y ^ c) / (d * x ^ e * y ^ f)
    """

    def __init__(self):
        super().__init__()
        numbers = range(1, 10)
        a = random.randint(1, 9)
        b = random.choice(numbers)
        c = random.choice(numbers)
        d = random.choice(numbers)
        e = random.choice(numbers)
        f = random.choice(numbers)
        self.a = (a * x ** b * y ** c)
        self.b = (d * x ** e * y ** f)

    def element(self):
        return self.a / self.b

    def question_latex(self) -> str:
        # return "\\frac{%s}{%s}" % (latex(self.a), latex(self.b))
        return "%s \\div %s" % (latex(self.a), latex(self.b))

    def answer(self):
        return sympy.cancel(self.element())

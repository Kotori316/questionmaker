import random

import sympy

from questions import factor as f
from questions.factor import Factor
from questions.questions import Helper, Questions


class Expand(Questions):
    """
    Case of using factor, (ax + b)(cx + d).
    """

    def __init__(self, factor: Factor):
        super().__init__()
        self.factor = factor

    def answer(self):
        return sympy.expand(self.factor.element())

    def element(self):
        return self.factor.factor()

    def answer_latex(self):
        if isinstance(self.factor, f.Factor6):
            return self.factor.question_latex()
        else:
            return super().answer_latex()

    def question_latex(self):
        return self.factor.answer_latex()


class Expand1(Questions):
    """
    Case of (x + a)(x + b).
    """

    def __init__(self):
        super().__init__()
        f1 = f.Factor1()
        f2 = f.Factor1()
        i = self.rand_int()
        a: f.Factor1 = random.choice((f1, f2))
        a.a1 = i
        a.a2 = i
        self.f1 = f1
        self.f2 = f2
        self.sig = bool(random.randint(0, 1))

    def element(self):
        if self.sig:
            return self.f1.element() + self.f2.element()
        else:
            return self.f1.element() - self.f2.element()

    def answer(self):
        return sympy.expand(self.element())


class Expand2(Questions):
    """
    Case of a(x + b) + c(x + d)
    """
    from sympy import Rational

    def __init__(self, a: Rational, b: Rational, c: Rational, d: Rational):
        super().__init__()
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def element(self):
        from sympy.abc import x
        return self.a * (x + self.b) + self.c * (x + self.d)

    def question_latex(self):
        left = r"\left("
        right = r"\right)"
        from sympy.abc import x
        a = Helper.replace_one(self.a)
        c = Helper.replace_one(self.c, signed=True)
        return fr"{a} {left}{sympy.latex(x + self.b)}{right} {c} {left}{sympy.latex(x + self.d)}{right}"

    def answer(self):
        return sympy.expand(self.element())

import functools
import random

import sympy
from sympy.abc import x
from sympy.printing import latex

from questions.questions import Questions


class Quadratic(Questions):
    def element(self):
        raise NotImplementedError("Element method must be implemented.")

    def answer(self):
        return sympy.solve(self.element())

    def answer_latex(self):
        return "x = " + functools.reduce(lambda a1, a2: a1 + r", \ " + a2, map(latex, self.answer()))

    def question_latex(self):
        return latex(self.element()) + " = 0"


def add_pm(answers, default):
    a1, a2 = answers
    if not a1.has(sympy.Pow):
        return default
    root = abs(a1.args[1])
    if not (root ** 2).is_Integer:
        # root must be Mul.
        rational = a1.args[0]
        d = root.args[0]
        p, q = d.p, d.q
        return "x = \\frac{" + f"{latex(rational * q)} \\pm {latex(root * q)}" + "}{" + f"{latex(q)}" + "}"
    else:
        return "x = " + f"{latex(answers[0].args[0])} \\pm {latex(abs(answers[0].args[1]))}"


class Quadratic1(Quadratic):
    """
    Case of (x - a)(x - b) = 0
    Answers are a and b.
    """

    def __init__(self):
        super().__init__()
        self.a = random.randint(-9, 10)
        self.b = random.randint(-9, 10)

    def element(self):
        return sympy.expand((x - self.a) * (x - self.b))


class Quadratic2(Quadratic):
    """
    Case of (b^2 - 4ac) != 0
    Answers are (-b \pm sqrt(b^2 - 4ac)) / 2a.
    """

    def __init__(self):
        super().__init__()
        a = random.choices(range(1, 10), (8, 1, 1, 1, 1, 1, 1, 1, 1))[0]
        b = random.randint(-9, 10)
        c = random.randint(-9, 10)
        while (b ** 2 - 4 * a * c) <= 0 or sympy.sqrt(b ** 2 - 4 * a * c).is_Integer:
            b = random.randint(-9, 10)
            c = random.randint(-9, 10)
        self.a = a
        self.b = b
        self.c = c

    def element(self):
        return self.a * (x ** 2) + self.b * x + self.c

    def answer_latex(self):
        answers = self.answer()
        return add_pm(answers, super().answer_latex())


class Quadratic3(Quadratic):
    """
    Case of (x - a)^2 = b
    Answers are a \pm sqrt(b).
    """

    def __init__(self):
        super().__init__()
        self.a = self.rand_int()
        self.b = sympy.Rational(random.randint(1, 9))

    def element(self):
        return (x - self.a) ** 2 - self.b

    def question_latex(self):
        return latex((x - self.a) ** 2) + " = " + latex(self.b)

    def answer_latex(self):
        answers = self.answer()
        return add_pm(answers, super().answer_latex())


MAGIC = 20


class Quadratic4(Quadratic):
    """
    Case of c * (x - a)(x - b) = 0
    a is a fraction. b is int.
    """

    def __init__(self):
        super().__init__()
        self.a = self.rand_frac()
        self.b = self.rand_int()
        self.c = self.a.q
        while not (0 <= self.b * self.c - self.a.p <= MAGIC):
            self.a = self.rand_frac()
            self.b = self.rand_int()
            self.c = self.a.q

    def element(self):
        return sympy.expand(self.c * (x - self.a) * (x - self.b))


class Quadratic5(Quadratic):
    """
   Case of (ax - b)(cx - d) = 0
   Both a and b are fraction.
   Answers are b/a and d/c.
   """

    def __init__(self):
        super().__init__()
        a = self.rand_frac()
        b = self.rand_frac()
        self.a = a.q
        self.b = a.p
        self.c = b.q
        self.d = b.p
        while not (0 <= abs(self.a * self.d - self.c * self.b) <= MAGIC and self.a * self.c <= MAGIC):
            a = self.rand_frac()
            b = self.rand_frac()
            self.a = a.q
            self.b = a.p
            self.c = b.q
            self.d = b.p

    def element(self):
        return sympy.expand((x * self.a - self.b) * (x * self.c - self.d))


def make_quadratic() -> Quadratic:
    question_list = (Quadratic1, Quadratic2, Quadratic3, Quadratic4, Quadratic5)
    q = random.choices(question_list, (3, 3, 1, 2, 1))[0]
    return q()

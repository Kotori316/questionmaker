import random

import sympy
from sympy import latex
from sympy.ntheory.factor_ import divisors

from questions import Helper, Questions


class IntCalc(Questions):

    def __init__(self):
        super().__init__()
        __a1list = range(-9, 0)
        self.a1 = sympy.Integer(random.choice(__a1list))
        self.a2 = sympy.Integer(self.rand_int())

    def element(self):
        return self.a1 + self.a2

    def question_latex(self) -> str:
        s = "-" if self.a2 < 0 else "+"
        return f"{latex(self.a1)} {s} {latex(abs(self.a2))}"

    def answer(self):
        return self.a1 + self.a2


class FracCalc(Questions):
    def __init__(self):
        super().__init__()
        a1 = self.rand_frac()
        a2 = self.rand_frac()
        self.a1, self.a2 = (-1 * a1, a2)

    def element(self):
        return self.a1 + self.a2

    def question_latex(self) -> str:
        s = "-" if self.a2 < 0 else "+"
        return f"{latex(self.a1)} {s} {latex(abs(self.a2))}"

    def answer(self):
        return self.a1 + self.a2


class RootCalc(Questions):
    """
    b / a ^ {1 / 2} + (a * c ^ 2) ^ {1 / 2}
    """

    def __init__(self):
        super().__init__()
        a_list = (2, 3, 5, 6, 7)
        self.a = random.choice(a_list)
        self.b = self.rand_int()
        self.c = random.choice(range(1, 7))

    def element(self):
        a = sympy.sqrt(self.a)
        return self.b / a + sympy.sqrt(self.a * self.c ** 2)

    def question_latex(self) -> str:
        s1 = "" if self.b >= 0 else "-"
        p1 = "%s \\frac{%s}{%s}" % (s1, str(abs(self.b)), latex(sympy.sqrt(self.a)))
        p2 = "+ \\sqrt{%s}" % (str(self.a * self.c ** 2))
        return p1 + p2

    def answer(self):
        return self.element()


class PMCalc(Questions):

    @staticmethod
    def _sig(r: sympy.Rational) -> str:
        if r > 0:
            return "+" + latex(r)
        else:
            return latex(r)

    def __init__(self, a: sympy.Rational, b: sympy.Rational, c: sympy.Rational, first_minus: bool):
        super().__init__()
        self.a = a
        self.b = b
        self.c = c
        if first_minus:
            s = r"\left(%s\right) - \left(%s\right) + %s"
            self.c = abs(self.c)
            self.question = s % (PMCalc._sig(a), PMCalc._sig(b), latex(self.c))
            self.e = self.a - self.b + self.c
        else:
            s = r"%s + \left(%s\right) - \left(%s\right)"
            self.a = abs(self.a)
            self.question = s % (latex(self.a), PMCalc._sig(b), PMCalc._sig(c))
            self.e = self.a + self.b - self.c

    def element(self):
        return self.e

    def answer(self):
        return self.e

    def question_latex(self) -> str:
        return self.question

    @staticmethod
    def make():
        a_p = Helper.rand_int()
        b_p = Helper.rand_int()
        c_p = Helper.rand_int()
        ns = set(range(1, 9))
        n = random.choice(list(ns.difference(divisors(a_p))))
        m = random.choice(list(ns.difference(divisors(b_p))))
        nm = n * m
        a = sympy.Rational(a_p, n)
        b = sympy.Rational(b_p, m)
        c = sympy.Rational(c_p, nm)
        d = bool(random.randint(0, 1))
        return PMCalc(a, b, c, d)

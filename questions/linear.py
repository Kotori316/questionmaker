import numpy as np
import sympy
from sympy import latex
from sympy.abc import x, y

from questions.questions import Helper, Questions


class Linears(Questions):

    def eq1(self) -> sympy.Eq:
        raise NotImplementedError

    def eq2(self) -> sympy.Eq:
        raise NotImplementedError

    def element(self):
        return self.eq1(), self.eq2()

    def answer(self):
        return sympy.solve(self.element(), x, y)

    def answer_latex(self) -> str:
        a = self.answer()
        return "x = {},\\ y = {}".format(latex(a[x]), latex(a[y]))

    def question_latex(self) -> str:
        return "\\left(\\begin{array}{rl} %s \\\\ %s \\end{array} \\right ." \
               % (latex(self.eq1().lhs) + " &= " + latex(self.eq1().rhs),
                  latex(self.eq2().lhs) + " &= " + latex(self.eq2().rhs))


class Linear1(Linears):
    """
    Case of ax + by = c, dx + ey = f
    x, y = n, m
    """

    def __init__(self):
        super().__init__()
        number_list = self.int_list
        import random
        arr = np.random.randint(1, 9, (2, 2))
        while np.linalg.det(arr) == 0:
            arr = np.random.randint(1, 9, (2, 2))
            arr[0][0] *= random.choice((1, -1))
            arr[0][1] *= random.choice((1, -1))
            arr[1][0] *= random.choice((1, -1))
            arr[1][1] *= random.choice((1, -1))
        self.x = random.choice(number_list)
        self.y = random.choice(number_list)
        e1 = arr[0][0] * x + arr[0][1] * y
        e2 = arr[1][0] * x + arr[1][1] * y
        self.e1 = sympy.Eq(e1, e1.subs([(x, self.x), (y, self.y)]))
        self.e2 = sympy.Eq(e2, e2.subs([(x, self.x), (y, self.y)]))

    def eq1(self) -> sympy.Eq:
        return self.e1

    def eq2(self) -> sympy.Eq:
        return self.e2


class Linear2(Linears):
    """
    Case of ax + by = c, y = dx + e
    """

    def __init__(self):
        super().__init__()
        r = self.int_list
        import random
        x0 = random.choice(r)
        y0 = random.choice(r)
        e = random.choice(r)
        d = sympy.Rational(y0 - e, x0)

        a = random.choice(r)
        b = random.choice(r)
        c = a * x0 + b * y0
        self.e1 = sympy.Eq(a * x + b * y, c)
        self.e2 = sympy.Eq(y, d * x + e)
        while len(sympy.solve((self.e1, self.e2))) == 0:
            a = random.choice(r)
            b = random.choice(r)
            c = random.choice(r)
            d = random.choice(r)
            e = random.choice(r)
            self.e1 = sympy.Eq(a * x + b * y, c)
            self.e2 = sympy.Eq(y, d * x + e)

    def eq1(self) -> sympy.Eq:
        return self.e1

    def eq2(self) -> sympy.Eq:
        return self.e2

    def question_latex(self) -> str:
        return "\\left(\\begin{array}{l} %s \\\\ %s \\end{array} \\right ." \
               % (latex(self.eq1().lhs) + " = " + latex(self.eq1().rhs),
                  latex(self.eq2().lhs) + " = " + latex(self.eq2().rhs))


class Polynomial(Questions):
    def element(self):
        return sympy.Eq(self.eq())

    def answer(self):
        return sympy.solve(self.element())

    def eq(self):
        raise NotImplementedError

    def answer_latex(self) -> str:
        a = self.answer()
        return f"x = {', '.join(list(map(latex, a)))}"


class Polynomial1(Polynomial):
    """
    Case ax + b = 0, where a and b are fraction.
    """

    def __init__(self):
        super().__init__()
        self.a = self.rand_frac()
        self.b = self.rand_frac()

    def eq(self):
        return x * self.a + self.b


class Polynomial2(Polynomial):
    """
    Case ax + b = 0, where a and b are int.
    """

    def __init__(self):
        super().__init__()
        self.a = self.rand_int()
        self.b = self.rand_int()

    def eq(self):
        return x * self.a + self.b


class Polynomial3(Polynomial):
    def __init__(self, e1, e2):
        super().__init__()
        self.e1 = e1
        self.e2 = e2

    def eq(self):
        return sympy.Eq(self.e1, self.e2)

    def element(self):
        return self.eq()


class Polynomial4(Polynomial3):
    """
    Case of y = a * x + b, where a, x and y are already known.
    """

    def __init__(self, a, x_1, y_1):
        b = sympy.Symbol("b")
        super().__init__(y_1, a * x_1 + b)
        self.a = a
        self.x_1 = x_1
        self.y_1 = y_1

    def question_latex(self) -> str:
        a = Helper.replace_one(self.a, signed=True, remove_one=False)
        x1 = Helper.replace_one(self.x_1, signed=True, remove_one=False)
        return fr"{self.y_1} = \left({a} \right) \times \left({x1} \right) + b" + r"\\ & bについて解く"
        # return r"%d = %d \times %d + b" % (self.y_1, self.a, self.x_1)

    def answer_latex(self):
        a = self.answer()
        return f"b = {latex(a[0])}"


class Polynomial5(Polynomial3):
    """
    Case of y = a * x + b, where b, x and y are already known.
    """

    def __init__(self, b, x_1, y_1):
        a = sympy.Symbol("a")
        super().__init__(y_1, a * x_1 + b)
        self.b = b
        self.x_1 = x_1
        self.y_1 = y_1

    def question_latex(self) -> str:
        x1 = Helper.replace_one(self.x_1, signed=True, remove_one=False)
        b = Helper.replace_one(self.b, signed=True, remove_one=False)
        return fr"{self.y_1} = a \times \left({x1} \right) {b}" + r"\\ & aについて解く"

        # return r"%d = a \times %d %s" % (self.y_1, self.x_1, "{:+}".format(self.b))

    def answer_latex(self):
        a = self.answer()
        return f"a = {latex(a[0])}"


class LinearFunction1(Questions):
    """
    Case of y = ax + b, where a and a pair of (x, y) are given.
    """

    def __init__(self, a, x1, y1):
        super().__init__()
        self.a = a
        self.x1 = x1
        self.y1 = y1

    def element(self):
        b = sympy.abc.b
        return sympy.Eq(y, self.a * x + b)

    def answer(self):
        eq = self.element().subs({x: self.x1, y: self.y1})
        ans = sympy.solve(eq, sympy.abc.b, dict=True)
        return ans[0][sympy.abc.b]

    def answer_latex(self) -> str:
        eq = self.element().subs(sympy.abc.b, self.answer())
        return sympy.latex(eq)

    def question_latex(self) -> str:
        s = f"1次関数で傾きが{latex(self.a)}"
        s1 = fr"\\ & 点({latex(self.x1)}, {latex(self.y1)})を通る直線の式は?"
        return s + s1
        # return fr"y = {Helper.replace_one(self.a)} x + b"

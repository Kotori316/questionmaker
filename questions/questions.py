import random
from typing import List

import sympy
from sympy.printing import latex


class Questions:

    def __init__(self):
        self.frac_list: List[sympy.Rational] = tuple(set(
            [f for f in [sympy.Rational(i1, i2) for i1 in range(1, 10) for i2 in range(1, 10)]
             if not f.is_integer]
        ))
        self.int_list: List[int] = tuple([i for i in range(-9, 10) if i != 0])

    def element(self):
        raise NotImplementedError("Element method must be implemented.")

    def answer(self):
        raise NotImplementedError("Answer method must be implemented.")

    def answer_latex(self) -> str:
        return latex(self.answer())

    def question_latex(self) -> str:
        return latex(self.element())

    def __str__(self):
        return self.question_latex() + " -> " + self.answer_latex().replace(r"\ ", "")

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if other is None or type(self) != type(other):
            return False
        return self.__dict__ == other.__dict__

    def __hash__(self):
        i = 0
        for k in self.__dict__:
            i ^= hash(self.__dict__[k])
        return hash(type(self))

    def rand_frac(self) -> sympy.Rational:
        return random.choice(self.frac_list) * random.choice((1, -1))

    def rand_int(self) -> int:
        return random.choice(self.int_list)


class Dummy(Questions):
    def __init__(self, data):
        super().__init__()
        self.data = str(data)

    def element(self):
        return self.data

    def answer(self):
        return self.data

    def answer_latex(self) -> str:
        return self.data

    def question_latex(self) -> str:
        return self.data


class Helper:
    frac_list: List[sympy.Rational] = tuple(set(
        [f for f in [sympy.Rational(i1, i2) for i1 in range(1, 10) for i2 in range(1, 10)]
         if not f.is_integer]
    ))
    int_list: List[int] = tuple([i for i in range(-9, 10) if i != 0])

    @staticmethod
    def rand_frac() -> sympy.Rational:
        return random.choice(Helper.frac_list) * random.choice((1, -1))

    @staticmethod
    def rand_int() -> int:
        return random.choice(Helper.int_list)

    @staticmethod
    def replace_one(a: sympy.Rational, signed=False, remove_one=True) -> str:
        if remove_one and abs(a) == 1:
            return ("+" if signed else "") if a > 0 else "-"
        else:
            if signed and a >= 0:
                return "+" + latex(a)
            else:
                return latex(a)

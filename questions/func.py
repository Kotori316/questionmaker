import itertools
from typing import Union

from sympy import Eq, Rational, divisors, latex
from sympy.abc import x, y

from questions import Questions


class Proportion(Questions):
    """
    Case of y = ax. A pair of (x, y) is given and get another y when other x is given.
    """

    def __init__(self, x1: Union[Rational, int], y1: Union[Rational, int], x2: Union[Rational, int]):
        super().__init__()
        self.x1 = Rational(x1)
        self.y1 = Rational(y1)
        self.x2 = Rational(x2)

    def element(self):
        return Eq(y, x * self.y1 / self.x1)

    def question_latex(self) -> str:
        return fr"yはxに比例する\\ & x={latex(self.x1)}のときy={latex(self.y1)}である \\ & x = {latex(self.x2)}のときのyは?"

    def answer(self):
        return self.y1 / self.x1 * self.x2

    def answer_latex(self):
        return "y = " + super().answer_latex()


class InverseProportion(Questions):
    """
    Case of a = xy. A pair of (x, y) is given and get another y when other x is given.
    """

    def __init__(self, x1: Union[Rational, int], y1: Union[Rational, int], x2: Union[Rational, int]):
        super().__init__()
        self.x1 = Rational(x1)
        self.y1 = Rational(y1)
        self.x2 = Rational(x2)
        self.a = self.x1 * self.y1

    def element(self):
        return Eq(y, self.a / x)

    def answer(self):
        return self.a / self.x2

    def answer_latex(self) -> str:
        return "y = " + super().answer_latex()

    def question_latex(self) -> str:
        return fr"yはxに反比例する\\ & x={latex(self.x1)}のときy={latex(self.y1)}である \\ & x = {latex(self.x2)}のときのyは?"

    @staticmethod
    def make():
        import random as ran
        x1 = ran.randint(1, 9) * ran.randrange(-1, 1 + 1, 2)
        y1 = ran.randint(1, 9) * ran.randrange(-1, 1 + 1, 2)
        a = x1 * y1
        divs = itertools.chain.from_iterable([(i, -i) for i in divisors(a)])
        x2 = ran.choice([i for i in divs if i != x1 and i != 1])
        return InverseProportion(x1, y1, x2)


class QuadraticProportion(Questions):
    """
    Case of y = ax^2. A pair of (x, y) is given and get another y when other x is given.
    """

    def __init__(self, x1: Union[Rational, int], y1: Union[Rational, int], x2: Union[Rational, int]):
        super().__init__()
        self.x1 = Rational(x1)
        self.y1 = Rational(y1)
        self.x2 = Rational(x2)
        self.a = self.y1 / self.x1 ** 2

    def element(self):
        return Eq(y, self.a * x ** 2)

    def answer(self):
        return self.a * self.x2 ** 2

    def answer_latex(self):
        return "y = " + super().answer_latex()

    def question_latex(self) -> str:
        return fr"yはxの2乗に比例する\\ & x={latex(self.x1)}のときy={latex(self.y1)}である \\ & x = {latex(self.x2)}のときのyは?"

    @staticmethod
    def make():
        import random as ran
        a = ran.randint(1, 5) * ran.randrange(-1, 1 + 1, 2)
        x_1 = ran.randint(1, 5) * ran.randrange(-1, 1 + 1, 2)
        y_1 = a * x_1 ** 2
        x_2 = ran.randint(-7, 9)
        return QuadraticProportion(x_1, y_1, x_2)

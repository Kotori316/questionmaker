from typing import Union

import sympy
from sympy import Rational
from sympy.abc import x, y

from questions import Questions


class Sub(Questions):
    """
    Case of y = f(x) and x is given.
    """

    def element(self):
        return sympy.Eq(y, self.fx)

    def answer(self):
        return self.fx.subs(x, self.x_1)

    def question_latex(self):
        s = super().question_latex()
        s2 = fr"\\  & x = {sympy.latex(self.x_1)} を代入"
        return s + s2

    def answer_latex(self) -> str:
        return "y = " + super().answer_latex()

    def __init__(self, fx: sympy.Eq, x_1):
        super().__init__()
        self.fx = fx
        self.x_1 = x_1


class Sub1(Questions):
    """
    Case of y = ax + b, where a and b are known. Value x is given.
    """

    def __init__(self, a: Union[Rational, int], b: Union[Rational, int], x_1: Union[Rational, int]):
        super().__init__()
        self.a = a
        self.b = b
        self.x_1 = x_1

    def element(self):
        return sympy.Eq(y, self.a * x + self.b)

    def answer(self):
        return self.element().rhs.subs(x, self.x_1)

    def answer_latex(self) -> str:
        return "y = " + super().answer_latex()

    def question_latex(self):
        s = super().question_latex()
        # s1 = " において、x = {}のときのyの値は?".format(sympy.latex(self.x_1))
        s2 = fr"\\  & x = {sympy.latex(self.x_1)} を代入"
        return s + s2


class Sub2(Questions):
    """
    Case of y = ax^2, where a is known and x is given.
    """

    def __init__(self, a: Union[Rational, int], x_1: Union[Rational, int]):
        super().__init__()
        self.a = a
        self.x_1 = x_1

    def element(self):
        return sympy.Eq(y, self.a * x ** 2)

    def answer(self):
        return self.element().rhs.subs(x, self.x_1)

    def answer_latex(self) -> str:
        return "y = " + super().answer_latex()

    def question_latex(self) -> str:
        s = super().question_latex()
        s2 = fr"\\  & x = {sympy.latex(self.x_1)} を代入"
        return s + s2

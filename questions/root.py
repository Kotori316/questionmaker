import random as rm

import sympy

from questions.questions import Questions


class Root1(Questions):

    def __init__(self):
        super().__init__()
        i_list = [i for i in range(1, 11) if not sympy.sqrt(i).is_Integer]
        self.a = rm.choice(i_list)
        self.b = rm.choice(range(1, 10))
        self.c = rm.choice(range(1, 10))
        self.d = rm.choice(range(1, 10))

    def element(self):
        return sympy.sqrt(self.a * self.b ** 2) + sympy.sqrt(self.a * self.c ** 2) + sympy.sqrt(self.a * self.d ** 2)

    def answer(self):
        return self.element()

    def question_latex(self) -> str:
        return r"\sqrt{%d} + \sqrt{%d} + \sqrt{%d}" % (self.a * self.b ** 2, self.a * self.c ** 2, self.a * self.d ** 2)


class Root2(Questions):

    def __init__(self):
        super().__init__()
        a_list = [i for i in range(1, 11) if not sympy.sqrt(i).is_Integer]
        b_list = self.int_list
        self.a = rm.choice(a_list)
        self.b = rm.choice(b_list)
        self.c = rm.choice(b_list)
        self.d = rm.choice(b_list)
        self.r1 = sympy.sqrt(self.a) * self.b
        self.r2 = sympy.sqrt(self.a) * self.c
        self.r3 = sympy.sqrt(self.a) * self.d

    def element(self):
        return self.r1 + self.r2 + self.r3

    def answer(self):
        return self.element()

    def question_latex(self) -> str:
        s1 = "+" if self.r2 >= 0 else ""
        s2 = "+" if self.r3 >= 0 else ""
        return sympy.latex(self.r1) + s1 + sympy.latex(self.r2) + s2 + sympy.latex(self.r3)
